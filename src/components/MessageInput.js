import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addMessage } from './../actions/chatActions';
import './MessageInput.css';

class MessageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ''
    }

    this.onSend = this.onSend.bind(this);
    this.onChange = this.onChange.bind(this); 
  }

  onSend() {
    const { message } = this.state;
    if (message !== '') {
      const date = {
        user: this.props.user,
        avatar: this.props.avatar,
        created_at: new Date().toLocaleString(),
        message,
        marked_read: true,
      }
      this.props.addMessage(date);
      this.setState({
        message: ''
      })
    }
  }

  onChange(e) {
    this.setState({
      message: e.target.value,
    })
  }

  render() {
    return (
      <div className="message-input">
        <input
          type="text"
          placeholder="Type a message"
          onChange={this.onChange}
          value={this.state.message}
        />
        <button onClick={this.onSend}>Send</button>
      </div>
    )
  }
}

export default connect(null, { addMessage })(MessageInput);