import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageItem from './MessageItem';
import * as actions from './../actions/chatActions';
import { setCurrentMessage, showPage } from './../actions/modalActions';
import './MessageList.css';

class MessageList extends Component {
  constructor(props) {
    super(props);

    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onAddLike = this.onAddLike.bind(this);
    this.onDeleteLike = this.onDeleteLike.bind(this);
  }

  onEdit(message) {
    this.props.setCurrentMessage(message);
    this.props.showPage();
  }

  onDelete(id) {
    if (window.confirm('Delete this message ?')) {
      this.props.deleteMessage(id);
    }
  }

  onAddLike(id) {
    this.props.addLike(id, this.props.currentUser)
  }

  onDeleteLike(id) {
    this.props.deleteLike(id, this.props.currentUser);
  }

  createMessages() {
    const { messages } = this.props;
    let currentDate = messages[0].created_at.split(' ')[0];

    return messages.map(messageInfo => {
      const { id, created_at } = messageInfo;

      let date = created_at.split(' ')[0];
      const addSpliter = currentDate !== date || messages.indexOf(messageInfo) === 0;
      currentDate = date;

      return (
        <div key={id}>
          {addSpliter ? <div className="spliter">{created_at.split(' ')[0]}</div> : null}
          <MessageItem
            currentUser={this.props.currentUser}
            messageInfo={messageInfo}
            onEdit={this.onEdit}
            onDelete={this.onDelete}
            onAddLike={this.onAddLike}
            onDeleteLike={this.onDeleteLike}
            canEdit={messages.indexOf(messageInfo) === messages.length - 1}
          />
        </div>
      );
    })
  }

  render() {
    return (
      <div className="messages">
        {this.createMessages()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.chat.messages
  }
}

const mapDispatchToProps = {
  ...actions,
  setCurrentMessage,
  showPage
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);