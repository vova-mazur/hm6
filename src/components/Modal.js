import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './../actions/modalActions';
import { updateMessage } from './../actions/chatActions';
import './Modal.css';

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newMessageContent: '',
    }

    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if(newProps.currentMessage.message) {
      this.setState({
        newMessageContent: newProps.currentMessage.message
      })
    }
  }

  onCancel() {
    this.props.dropCurrentMessage();
    this.props.hidePage();
    this.setState({
      newMessageContent: ''
    })
  }

  onSave() {
    const { newMessageContent } = this.state;
    if (newMessageContent !== '') {
      const { id } = this.props.currentMessage;
      this.props.updateMessage(id, newMessageContent);
      this.props.dropCurrentMessage();
      this.props.hidePage();
      this.setState({
        newMessageContent: ''
      });
    } else {
      alert('The message can not be empty!')
    }
  }

  onChange(e) {
    this.setState({
      newMessageContent: e.target.value
    })
  }

  getModalContent() {
    return (
      <div className="modal">
        <h3>Edit message</h3>
        <textarea value={this.state.newMessageContent} onChange={this.onChange}></textarea>
        <div className="buttons">
          <button onClick={this.onSave}>Ok</button>
          <button onClick={this.onCancel}>Cancel</button>
        </div>
      </div>
    )
  }

  render() {
    const { isShown } = this.props;
    return isShown ? this.getModalContent() : null;
  }
}

const mapsStateToProps = state => {
  const { isShown, currentMessage } = state.modal;
  return {
    isShown,
    currentMessage,
  }
}

const mapDispatchToProps = {
  ...actions,
  updateMessage
}

export default connect(mapsStateToProps, mapDispatchToProps)(Modal);
