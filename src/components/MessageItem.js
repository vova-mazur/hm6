import React, { Component } from 'react'

export default class MessageItem extends Component {
  constructor(props) {
    super(props);

    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleKeyPress(event) {
    if (event.keyCode !== 38) {
      return
    }

    this.props.onEdit(this.props.messageInfo);
  }

  componentWillMount() {
    if (this.props.canEdit) {
      document.addEventListener('keydown', this.handleKeyPress)
    }
  }

  componentWillUnmount() {
    if (this.props.canEdit) {
      document.removeEventListener('keydown', this.handleKeyPress);
    }
  }

  render() {
    const { id, user, avatar, created_at, message, marked_read, likes } = this.props.messageInfo;

    return (
      <div className="message">
        {marked_read ? null : <img src={avatar} alt="avatar" />}
        <div className="content">
          <div className="title">
            <p className="author">{user}</p>
            <p className="data">{created_at}</p>
          </div>
          <p className="text">{message}</p>
        </div>
        {
          marked_read ?
            <div>
              {this.props.canEdit
                ? <button className="hidden"onClick={(e) => this.props.onEdit(this.props.messageInfo)}>Edit</button>
                : null}
              <button onClick={(e) => this.props.onDelete(id)}>Delete</button>
            </div> :
            <div>
              {
                likes && likes.includes(this.props.currentUser) ?
                  <button onClick={(e) => this.props.onDeleteLike(id)}>Unlike</button> :
                  <button onClick={(e) => this.props.onAddLike(id)}>Like</button>
              }
            </div>
        }
      </div>
    )
  }
}
