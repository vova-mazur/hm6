import { 
  SET_MESSAGES, 
  ADD_MESSAGE, 
  DELETE_MESSAGE, 
  UPDATE_MESSAGE,
  ADD_LIKE,
  DELETE_LIKE
} from './../actions/types';

const initialState = {
  loading: true,
  messages: [],
}

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_MESSAGES: {
      return {
        loading: false,
        messages: action.payload,
      }
    }
    case ADD_MESSAGE: {
      const { id, data } = action.payload;
      const newMessage = { id, ...data };
      return {
        ...state,
        messages: [
          ...state.messages, 
          newMessage
        ]
      }
    }
    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.messages.filter(message => message.id !== id);
      return {
        ...state,
        messages: filteredMessages
      };
    }
    case UPDATE_MESSAGE: {
      const { id, newContent } = action.payload;
      const updatedMessages = state.messages.map(msg => {
        if(msg.id === id) {
          return {
            ...msg,
            message: newContent,
          }
        } else {
          return msg;
        }
      })
      return {
        ...state,
        messages: updatedMessages
      };
    }
    case ADD_LIKE: {
      const { id, username } = action.payload;
      const updatedMessages = state.messages.map(message => {
        if(message.id === id) {
          if(message.likes) {
            message.likes.push(username);
          } else {
            message.likes = [username];
          }
        } 
        return message;
      })

      return {
        ...state,
        messages: updatedMessages
      };
    }
    case DELETE_LIKE: {
      const { id, username } = action.payload;
      const updatedMessages = state.messages.map(message => {
        if(message.id === id) {
          message.likes = message.likes.filter(name => name !== username);
        }
        return message;
      })

      return {
        ...state,
        messages: updatedMessages
      };
    }
    default:
      return state;
  }
}