import { combineReducers } from 'redux';
import chatReducer from './chatReducer';
import modalReducer from './modalReducer';

export default combineReducers({
  chat: chatReducer,
  modal: modalReducer
});