import { 
  SET_CURRENT_MESSAGE, 
  DROP_CURRENT_MESSAGE, 
  SHOW_PAGE, 
  HIDE_PAGE 
} from './../actions/types';

const intialState = {
  currentMessage: {},
  isShown: false,
};

export default function(state = intialState, action) {
  switch(action.type) {
    case SET_CURRENT_MESSAGE: {
      const { message } = action.payload;
      return {
        ...state,
        currentMessage: message
      }
    }
    case DROP_CURRENT_MESSAGE: {
      return {
        ...state,
        currentMessage: {}
      }
    }
    case SHOW_PAGE: {
      return {
        ...state,
        isShown: true
      }
    }
    case HIDE_PAGE: {
      return {
        ...state,
        isShown: false
      }
    }
    default:
      return state;
  } 
}