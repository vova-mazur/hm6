import { 
  SET_CURRENT_MESSAGE, 
  DROP_CURRENT_MESSAGE, 
  SHOW_PAGE, 
  HIDE_PAGE 
} from './types';

export const setCurrentMessage = message => ({
  type: SET_CURRENT_MESSAGE,
  payload: {
    message
  }
});

export const dropCurrentMessage = () => ({
  type: DROP_CURRENT_MESSAGE
});

export const showPage = () => ({
  type: SHOW_PAGE
})

export const hidePage = () => ({
  type: HIDE_PAGE
})