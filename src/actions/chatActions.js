import {
  SET_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
  ADD_LIKE,
  DELETE_LIKE,
} from './types';
import service from './../services/idService';

export const setMessages = messages => ({
  type: SET_MESSAGES,
  payload: messages
});

export const addMessage = data => ({
  type: ADD_MESSAGE,
  payload: {
    id: service.getNewId(),
    data
  }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});

export const updateMessage = (id, newContent) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    newContent
  }
});

export const addLike = (id, username) => ({
  type: ADD_LIKE,
  payload: {
    id,
    username
  }
});

export const deleteLike = (id, username) => ({
  type: DELETE_LIKE,
  payload: {
    id,
    username
  }
});