import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from './logo.svg';
import './App.css';
import { setMessages } from './actions/chatActions';

import Header from './components/Header';
import MessageList from './components/MessageList';
import MessageInput from './components/MessageInput';
import Modal from './components/Modal';

class Chat extends Component {
  async componentWillMount() {
    const response = await fetch('https://api.myjson.com/bins/1hiqin');
    const messages = await response.json();
    this.props.setMessages(messages);
  }

  render() {
    return (
      <div className="Chat">
        {this.props.loading
          ? <img src={logo} className="App-logo" alt="logo" />
          : <div>
              <Modal />
              <Header messages={this.props.messages} />
              <MessageList currentUser={this.props.user} />
              <MessageInput user={this.props.user} avatar={this.props.avatar} />
            </div>
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.chat.loading,
    messages: state.chat.messages
  }
}

const mapDispatchToProps = {
  setMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)